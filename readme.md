# Node, Redis and Mongo Backend 
This is an API server for users. In this project you are able to do all type of CRUD actions on users. I am using mongo nosql database for storing the data. Also I am using redis for caching some endpoints. You can run BDD tests or postman tests.

## Technology used:
* Node.js
* Mongoose.js
* Redis.js
* jwt authentication
* Docker

## How to run and setup?
* Run `npm install` to install all node dependencies.
* Run `docker-compose up` in a terminal. With this command you will have both mongo and redis server in your environment.
* if have any error connect to redis server so you can Run `npm start` or `node server.js` using another terminal on project directory.
* **You are done.** You can go `http://localhost:8081` and check if everything is up and running.
* Read documentation for the API here : https://documenter.getpostman.com/view/11356963/Szzhdxm6?version=latest

## How to test?
    * Get access token
        post "/fajar-basoni/api/users/GenerateToken"
    * request body
        {
            "userName" : "pulisic"
        }

    * Create new users
        post "/fajar-basoni/api/users/Create"
    * request body
        {
            "userName": "fajar.basoni",
            "accountNumber": "00001",
            "emailAddress": "fajar.basoni@gmail.com",
            "identityNumber": "3201020101910002"
        }

    * Get all users with auth
        get "/fajar-basoni/api/users/GetAll"
    * request header
        Authorization: value from //Get access token
    
    * Get users by account number with auth
        get "/fajar-basoni/api/users/GetByAccountNumber"
    * requset body
        {
            "accountNumber":"00001"
        }
    * request header
        Authorization: value from //Get access token
    
    * Get users by identity number with auth
        get "/fajar-basoni/api/users/GetByIdentityNumber"
    requset body
        {
            "identityNumber":"3201020101910003"
        }
    request header
        Authorization: value from //Get access token
        
    * Update data users by id with auth
        put "/fajar-basoni/api/users/Update/:id"
    * request param
        5ee05b979574971c883e8117 //:id
    * request body
        {
            "userName": "pedro.rodriguez",
            "accountNumber": "00014",
            "emailAddress": "pedro.rodriguez@chelsea.co.uke",
            "identityNumber": "3201020101910000"
        }
    * request header
        Authorization: value from //Get access token
    
    * Delete data users by id with auth
        delete "/fajar-basoni/api/users/Delete/:id"
    * request param
        5ee05b979574971c883e8117 //:id
    * request header
        Authorization: value from //Get access token
    
    * Delete all users with auth
        delete "/fajar-basoni/api/users/DeleteAll"
    * request header
        Authorization: value from //Get access token
    
## How is cache working?
As i mentioned before, redis is used for caching. I have a services for the manage endpoint. 

for faster run backend need cache when load the data. The cache logic is simple, whenever you made a request, i am creating a key with those query parameters. It is sth like _ seperated string. Then we are caching the whole result as JSON on redis.

When we get a request, our redis cache service is always working and if there is cache for this query, we are parsing this string into JSON format and send the result back.

for synchronize data mongodb with redis cache when any modify in mongodb. after success modified data so on this system will clear cache data. and update with data from mongodb.

## How access token:
To generate the token here the token request will be handled by endpoin / GenerateToken. requires request body {userName: "value"} to generate a new token which is active for 24 hours.

So when accessing endpoint /create it does not require tokens to be considered as user registrations that can later access each endpoint, in fact to be more secure again, in the collection user requires a password as the authentication user that will access. 

But for other endpoints it requires access token which will be input into the request header with the Authorization key: "value_token". 

Function to verify token is in the auth.middleware.js middleware that will be adjusted to the secret key in auth.config.js. when successful the endpoint can be accessed. otherwise it won't be able to access endpoints.