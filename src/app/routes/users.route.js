module.exports = app => {
    const users = require("../controllers/users.controller.js");
    const AuthorizationController = require("../controllers/auth.controller.js");
    const AuthToken = require("../middleware/auth.middleware.js");
    var router = require("express").Router();

    //Get access token
    router.post('/GenerateToken', [AuthorizationController.signin]);
    //Create new users
    router.post("/Create", users.create);
    //Get all users with auth
    router.get("/GetAll",[AuthToken.verifyToken, users.findAll]);
    //Get users by account number with auth
    router.get("/GetByAccountNumber", [AuthToken.verifyToken, users.findByaccountNumber]);
    //Get users by identity number with auth
    router.get("/GetByIdentityNumber",[AuthToken.verifyToken, users.findByidentityNumber]);
    //Update data users by id with auth
    router.put("/Update/:id", [AuthToken.verifyToken, users.update]);
    //Delete data users by id with auth
    router.delete("/Delete/:id", [AuthToken.verifyToken, users.delete]);
    //Delete all users with auth
    router.delete("/DeleteAll/", [AuthToken.verifyToken, users.deleteAll]);

    app.use('/fajar-basoni/api/users', router);
}