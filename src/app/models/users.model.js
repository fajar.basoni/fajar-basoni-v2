module.exports = mongoose => {
  var schema = mongoose.Schema({
    id: String,
    userName: String, 
    accountNumber: String, 
    emailAddress: String, 
    identityNumber: String
  });

  const Users = mongoose.model("users", schema);
  return Users;
};

  