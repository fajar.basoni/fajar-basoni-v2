const db = require("../models");
const Users = db.users;
const clearCache = require('../services/cache.service');

// Create and Save a new users
exports.create = (req, res) => {
    //Validate request
    if (!req.body.userName
        || !req.body.accountNumber
        || !req.body.emailAddress
        || !req.body.identityNumber) {
        return res.status(400).send({ message: "Invalid request!!" })
    }

    const users = new Users({
        userName: req.body.userName,
        accountNumber: req.body.accountNumber,
        emailAddress: req.body.emailAddress,
        identityNumber: req.body.identityNumber
    });

    //save data user
    users
        .save(users)
        .then(data => {
            clearCache();
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "some error occured during create users."
            });
        });
};

// Retrieve all users from the database.
exports.findAll = (req, res) => {
    const userName = req.query.userName;
    var conditions = userName ? { userName: { $regex: new RegExp(userName), $options: "i" } } : {};

    Users.find(conditions)
        .cache(req.query.userName)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occured during retrieve users."
            });
        });
};

// Find a single users by account number
exports.findByaccountNumber = (req, res) => {
    const accountNumber = req.body.accountNumber;
    Users.findOne({ accountNumber: accountNumber })
        .cache(accountNumber)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Users with account number " + accountNumber });
            else res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occured during get users by account Number."
            });
        });
};

// Find a single users by identity number
exports.findByidentityNumber = (req, res) => {
    const identityNumber = req.body.identityNumber;
    Users.findOne({ identityNumber: identityNumber })
        .cache(identityNumber)
        .then(data => {
            if (!data)
                res.status(404).send({ message: "Not found Users with identity number " + identityNumber });
            else res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occured during get users by identity Number."
            });
        });
};

// Update a users by the id in the request
exports.update = (req, res) => {
    //Validate request
    if (!req.body.userName
        && !req.body.accountNumber
        && !req.body.emailAddress
        && !req.body.identityNumber) {
            return res.status(400).send(
                {message: "Data to update can not be empty!"}
            );
        }

    const id = req.params.id;

    Users.findByIdAndUpdate(id, req.body, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Users with id=${id}. Maybe Users was not found!`
                });
            } else {
                clearCache();
                res.send({ message: `Users with id=${id} was updated successfully.` });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Users with id=" + id
            });
        });
};

// Delete a users with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;

    Users.findByIdAndRemove(id, { useFindAndModify: false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete Users with id=${id}. Maybe Users was not found!`
                });
            } else {
                clearCache();
                res.send({
                    message: `Users with id=${id} was deleted successfully!`
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Users with id=" + id
            });
        });
};

// Delete all users from the database.
exports.deleteAll = (req, res) => {
    Users.deleteMany({})
        .then(data => {
            clearCache();
            res.send({
                message: `${data.deletedCount} Users were deleted successfully!`
            });
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while removing all users."
            });
        });
};